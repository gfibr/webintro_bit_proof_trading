/* tslint:disable */
/* eslint-disable */
/**
 * FP Order service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.4
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface TradePurchase
 */
export interface TradePurchase {
    /**
     * 
     * @type {number}
     * @memberof TradePurchase
     */
    id: number;
    /**
     * 
     * @type {string}
     * @memberof TradePurchase
     */
    companyID?: string;
    /**
     * BID | ASK
     * @type {string}
     * @memberof TradePurchase
     */
    tradeType?: string;
    /**
     * 
     * @type {string}
     * @memberof TradePurchase
     */
    date?: string;
    /**
     * queue | ready | taken
     * @type {string}
     * @memberof TradePurchase
     */
    status?: string;
}

export function TradePurchaseFromJSON(json: any): TradePurchase {
    return TradePurchaseFromJSONTyped(json, false);
}

export function TradePurchaseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TradePurchase {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': json['id'],
        'companyID': !exists(json, 'CompanyID') ? undefined : json['CompanyID'],
        'tradeType': !exists(json, 'tradeType') ? undefined : json['tradeType'],
        'date': !exists(json, 'date') ? undefined : json['date'],
        'status': !exists(json, 'status') ? undefined : json['status'],
    };
}

export function TradePurchaseToJSON(value?: TradePurchase | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'CompanyID': value.companyID,
        'tradeType': value.tradeType,
        'date': value.date,
        'status': value.status,
    };
}


